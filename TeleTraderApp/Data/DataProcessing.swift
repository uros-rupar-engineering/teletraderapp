//
//  DataProcessing.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/17/21.
//

import Foundation
import UIKit


class DataProcessing{
    
    static var symbols:[Symbol] = []
    static var news:[News] = []
    
    
   
    
    
    static func getData(_ string:String){
        
        
        let jsonData  = string.data(using: .utf8)

        let  decoder = JSONDecoder()

        if let data = jsonData, let symbolsData = try? decoder.decode([Symbol].self, from: data){
     symbols = symbolsData
            
           
        }else{
            print("ddd")
        }
        
    }
    
    static func getNews(_ string:String){
        
        
        let jsonData  = string.data(using: .utf8)

        let  decoder = JSONDecoder()

        if let data = jsonData, let newsData = try? decoder.decode([News].self, from: data){
     news = newsData
            print(news)
           
        }else{
            print("ddd")
        }
        
    }
    
    
    
    
    
  
    static let jsonString = """
        [
        {
            "id" : "tts-394917",
            "name" : "BOW JONES INDUSTRIAL AVERAGE INDEX",
            "tickerSymbol" : "DJI",
            "isin" : "US78378X1072",
            "currency" : "XXP",
            "stockExchangeName" : "Indices S&P Basic",
            "decorativeName" : "SP500",
            "quote" : {
                "last" : 25720.66,
                "high" : 25800.30,
                "low": 25518.05,
                "bid": 25539.44,
                "ask": 25822.19,
                "volume": 248068144,
                "dateTime" : "2019-06-06T22:20:01",
                "change" : 181.09,
                "changePercent" : 0.709057
            }
        },
                {
                    "id" : "tts-394917",
                    "name" : "AOW JONES INDUSTRIAL AVERAGE INDEX",
                    "tickerSymbol" : "DJI",
                    "isin" : "US78378X1072",
                    "currency" : "XXP",
                    "stockExchangeName" : "Indices S&P Basic",
                    "decorativeName" : "SP500",
                    "quote" : {
                        "last" : 25720.66,
                        "high" : 25800.30,
                        "low": 15518.05,
                        "bid": 25539.44,
                        "ask": 25822.19,
                        "volume": 248068144,
                        "dateTime" : "2019-06-06T22:20:01",
                        "change" : 181.09,
                        "changePercent" : 0.809057
                    }
                },
                        {
                            "id" : "tts-394917",
                            "name" : "IBEX 35.",
                            "tickerSymbol" : "DJI",
                            "isin" : "US78378X1072",
                            "currency" : "XXP",
                            "stockExchangeName" : "Indices S&P Basic",
                            "decorativeName" : "SP500",
                            "quote" : {
                                "last" : 15760.66,
                                "high" : 25800.30,
                                "low": 24518.05,
                                "bid": 15539.44,
                                "ask": 4822.19,
                                "volume": 248068144,
                                "dateTime" : "2019-06-06T22:20:01",
                                "change" : 181.09,
                                "changePercent" : 0.129057
                            }
                        }
                        ,
                                {
                                    "id" : "tts-394917",
                                    "name" : "CAC 40",
                                    "tickerSymbol" : "DJI",
                                    "isin" : "US78378X1072",
                                    "currency" : "XXP",
                                    "stockExchangeName" : "Indices S&P Basic",
                                    "decorativeName" : "SP500",
                                    "quote" : {
                                        "last" : 25720.66,
                                        "high" : 25800.30,
                                        "low": 25518.05,
                                        "bid": 25539.44,
                                        "ask": 25822.19,
                                        "volume": 248068144,
                                        "dateTime" : "2019-06-06T22:20:01",
                                        "change" : 181.09,
                                        "changePercent" : -0.453057
                                    }
                                },
                        {
                            "id" : "tts-394917",
                            "name" : "UK 100 Index",
                            "tickerSymbol" : "DJI",
                            "isin" : "US78378X1072",
                            "currency" : "XXP",
                            "stockExchangeName" : "Indices S&P Basic",
                            "decorativeName" : "SP500",
                            "quote" : {
                                "last" : 25720.66,
                                "high" : 25800.30,
                                "low": 25518.05,
                                "bid": 25539.44,
                                "ask": 25822.19,
                                "volume": 248068144,
                                "dateTime" : "2019-06-06T22:20:01",
                                "change" : 181.09,
                                "changePercent" : -0.78057
                            }
                        },
                        {
                            "id" : "tts-394917",
                            "name" : "DAX",
                            "tickerSymbol" : "DJI",
                            "isin" : "US78378X1072",
                            "currency" : "XXP",
                            "stockExchangeName" : "Indices S&P Basic",
                            "decorativeName" : "SP500",
                            "quote" : {
                                "last" : 55720.66,
                                "high" : 1200.30,
                                "low": 25518.05,
                                "bid": 23539.44,
                                "ask": 2322.19,
                                "volume": 248068144,
                                "dateTime" : "2019-06-06T22:20:01",
                                "change" : 181.09,
                                "changePercent" : 6.7057
                            }
                        },
                        {
                            "id" : "tts-394917",
                            "name" : "S&P 500 INDEX",
                            "tickerSymbol" : "DJI",
                            "isin" : "US78378X1072",
                            "currency" : "XXP",
                            "stockExchangeName" : "Indices S&P Basic",
                            "decorativeName" : "SP500",
                            "quote" : {
                                "last" : 25720.66,
                                "high" : 25800.30,
                                "low": 25518.05,
                                "bid": 25539.44,
                                "ask": 25822.19,
                                "volume": 248068144,
                                "dateTime" : "2019-06-06T22:20:01",
                                "change" : 181.09,
                                "changePercent" : 20.7057
                            }
                        },
                            {
                                "id" : "tts-394917",
                                "name" : "SDB Italy 40 Index",
                                "tickerSymbol" : "DJI",
                                "isin" : "US78378X1072",
                                "currency" : "XXP",
                                "stockExchangeName" : "Indices S&P Basic",
                                "decorativeName" : "SP500",
                                "quote" : {
                                "last" : 25720.66,
                                "high" : 25800.30,
                                "low": 25518.05,
                                "bid": 25539.44,
                                "ask": 25822.19,
                                "volume": 248068144,
                                "dateTime" : "2019-06-06T22:20:01",
                                "change" : 181.09,
                                "changePercent" : -9.7057
                                        }
                                    },
                        {
                            "id" : "tts-394917",
                            "name" : "S&P/ASX 200",
                            "tickerSymbol" : "DJI",
                            "isin" : "US78378X1072",
                            "currency" : "XXP",
                            "stockExchangeName" : "Indices S&P Basic",
                            "decorativeName" : "SP500",
                            "quote" : {
                                "last" : 25720.66,
                                "high" : 25800.30,
                                "low": 25518.05,
                                "bid": 25539.44,
                                "ask": 25822.19,
                                "volume": 248068144,
                                "dateTime" : "2019-06-06T22:20:01",
                                "change" : 181.09,
                                "changePercent" : -3.7057
                            }
                        }

        
        ]
        
        """
    
    
   static let jsonNewsString = """
        [
            {
                "headLine" : "Russia wants to deploy satellites especially for IoT",
                "picture" : "https://www.sigfox.com/sites/default/files/news%20-%202020-03/Russia%20map%20elipse_0.png"
            },
        
                    {
                        "headLine" : "US stocks rise premarket despite trade worries",
                        "picture" : "https://www.meeting-point.com/wp-content/uploads/2019/09/America.jpg"
                    }
                    ,
                
                    {
                        "headLine" : "Failure of FCA-Renault merger bad for France - Di Maio",
                        "picture" : "https://www.sigfox.com/sites/default/files/news%20-%202020-03/Russia%20map%20elipse_0.png"
                            },
                        {
                            "headLine" : "output cuts likely to be extended by 2020 - minister",
                            "picture" : "https://lh3.googleusercontent.com/proxy/AHe_iSpmFSnjUXU6dYPyoEiQgM9fgaWKikT63HgC4BysMrVhoek5RPgzaCGxqVhZQKcSqXtrQFtCU6Gh6B3t_tXRavlwaWRf0wd13imlJ3M6a0k"
                                },
                    {
                        "headLine" : "S. Korea: Huawei's 5G not threat to natl security",
                        "picture" : "https://cdni0.trtworld.com/w960/h540/q75/52964_KOR190403SOUTHKOREAINTERNETAFP_1554279558263.jpg"
                            },
                        {
                            "headLine" : "Failure of FCA-Renault merger bad for France - Di Maio",
                            "picture" : "https://vrelegume.rs/wp-content/uploads/2021/03/7-3.jpg"
                                }
        ]
        
        """
}
