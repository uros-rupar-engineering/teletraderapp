//
//  CvVC.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/19/21.
//

import UIKit
import PDFKit

class CvVC: UIViewController {

    @IBOutlet weak var pdfFile: PDFView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = Bundle.main.path(forResource: "Uros's Resume", ofType: "pdf") {
                  if let pdfDocument = PDFDocument(url: URL(fileURLWithPath: path)) {
                    pdfFile.displayMode = .singlePageContinuous
                    pdfFile.autoScales = true
                    pdfFile.displayDirection = .vertical
                    pdfFile.document = pdfDocument
                  }
              }
          }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


