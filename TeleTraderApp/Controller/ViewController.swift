//
//  ViewController.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/15/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var nameColumn: UILabel!
    @IBOutlet weak var labelColumn1: UILabel!
    @IBOutlet weak var labelColumn2: UILabel!
    
    @IBOutlet weak var changeFormatButton: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    
    @IBOutlet  weak  var symbolsTable: UITableView!
    var formatIsProcentChange = true;
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return DataProcessing.symbols.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.tableFooterView = UIView()
        let cell = tableView.dequeueReusableCell(withIdentifier: "symbolCell", for: indexPath) as! SymbolCell
        
        cell.symbolNameLabel.text = DataProcessing.symbols[indexPath.row].name
        
        if formatIsProcentChange{
           
            
            cell.label1.text = String(DataProcessing.symbols[indexPath.row].quote.last)
            cell.label2.text = String(DataProcessing.symbols[indexPath.row].quote.changePercent) + "%"
            
            if DataProcessing.symbols[indexPath.row].quote.changePercent > 0{
                cell.label2.textColor = .green
                
            }else{
                cell.label2.textColor = .red
            }
        }else{
            cell.label1.textColor = .white
            cell.label2.textColor = .white
            cell.label1.text = " \(DataProcessing.symbols[indexPath.row].quote.bid) / \(DataProcessing.symbols[indexPath.row].quote.ask)  "
            cell.label2.text = " \(DataProcessing.symbols[indexPath.row].quote.high) / \(DataProcessing.symbols[indexPath.row].quote.low)  "
            
        }
        
        
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
     
        initializeUI()
     

       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        UITabBar.appearance().barTintColor = .black
        UITabBar.appearance().unselectedItemTintColor = UIColor.white
    }
    override func viewWillDisappear(_ animated: Bool) {
        UITabBar.appearance().barTintColor = .black
        UITabBar.appearance().unselectedItemTintColor = UIColor.white
    }
    
    @IBAction func changeFormatButton(_ sender: Any) {
        
        formatIsProcentChange = !formatIsProcentChange;
        initializeCaptreOfColumnTable()
        symbolsTable.reloadData()
        
    }
    
    func initializeCaptreOfColumnTable(){
        nameColumn.text = "name"
        if  formatIsProcentChange{
           
            labelColumn1.text = "last"
            labelColumn2.text = "change(%)"
        }else{
            labelColumn1.text = "bid/ask"
            labelColumn2.text = "high/low"
        }
        
        
    }
    
    
    
    func initializeUI(){
        symbolsTable.separatorColor = .systemGray4
        initializeCaptreOfColumnTable()
        
        roundButtons()
    }
    
    func roundButtons(){
        changeFormatButton.layer.cornerRadius = 10
        sortButton.layer.cornerRadius = 10
    }
    @IBAction func changeSort(_ sender: Any) {
        showAlertForSorting()
        symbolsTable.reloadData()
    }
    
    @IBAction func refreshData(_ sender: Any) {
        DataProcessing.getData(DataProcessing.jsonString)
        symbolsTable.reloadData()
    }
    
    
    
     func showAlertForSorting(){
        let alert = UIAlertController(title: "choose way of sorting", message: "unsei kupon", preferredStyle: .alert)
        
      
       
        
        alert.addAction(UIAlertAction(title: "ascending", style: UIAlertAction.Style.default,handler: sortArrayASC))
        
        alert.addAction(UIAlertAction(title: "descending", style: UIAlertAction.Style.default,handler:sortArrayDSC))
        
        alert.addAction(UIAlertAction(title: "default", style: UIAlertAction.Style.default,handler: sortArrayDefaultC))
        
        
        self.present(alert, animated: true)
        
    }
    
      func sortArrayASC(alert:UIAlertAction){
        DataProcessing.symbols = DataProcessing.symbols.sorted{ $0.name.compare($1.name, options: .literal) == .orderedAscending }
        symbolsTable.reloadData()
    }
    
      func sortArrayDSC(alert:UIAlertAction){
        DataProcessing.symbols = DataProcessing.symbols.sorted{ $0.name.compare($1.name, options: .literal) == .orderedDescending }
        symbolsTable.reloadData()
    
    }
    
      func sortArrayDefaultC(alert:UIAlertAction){
        DataProcessing.getData(DataProcessing.jsonString)
        symbolsTable.reloadData()
    }
    
    //For Deleting table row
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCell.EditingStyle.delete {
            DataProcessing.symbols.remove(at: indexPath.row)
              tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
          }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SymbolDetails"{
            let controller = segue.destination as! SymbolDetailsVC
            
            controller.currentSymbol = DataProcessing.symbols[symbolsTable.indexPathForSelectedRow!.row];
            
            symbolsTable.deselectRow(at: symbolsTable.indexPathForSelectedRow!, animated: false)

        }
    }
    


}

