//
//  NewsDetailsVC.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/19/21.
//

import UIKit

class NewsDetailsVC: UIViewController {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var headLine: UILabel!
    
    var currentNews:News!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        

        
        if let urlString = currentNews.picture as? String,let url = URL(string: urlString){
            URLSession.shared.dataTask(with: url){(data,response,error) in
                if let data = data{

                    DispatchQueue.main.async {
                        
                        self.mainImage.image = UIImage(data: data)
                        
                    }
                }
            }.resume()
     }
        headLine.text = currentNews.headLine
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
