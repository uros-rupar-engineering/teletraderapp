//
//  NewsVc.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/18/21.
//

import UIKit


class NewsVc: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var newsTable: UITableView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataProcessing.news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        if indexPath.row == 0{
           let cell = tableView.dequeueReusableCell(withIdentifier:"NewsCell2" , for: indexPath) as! MainNewsCellView
            
            tableView.rowHeight = view.frame.height * 0.35
                    cell.headLine.text = DataProcessing.news[indexPath.row].headLine
                    
                    if let urlString = DataProcessing.news[indexPath.row].picture as? String,let url = URL(string: urlString){
                        URLSession.shared.dataTask(with: url){(data,response,error) in
                            if let data = data{

                                DispatchQueue.main.async {
                                    
                                    cell.mainImage.image = UIImage(data: data)
                                    
                                }
                            }
                        }.resume()
                    
                    
                    }
            return cell
                }else{
                    tableView.rowHeight = view.frame.height * 0.14
                   let  cell =  tableView.dequeueReusableCell(withIdentifier:"NewsCell" , for: indexPath) as! NewsCell
                   
                   cell.headLineLabel.text = DataProcessing.news[indexPath.row].headLine
                  
                   
                   if let urlString = DataProcessing.news[indexPath.row].picture as? String,let url = URL(string: urlString){
                       URLSession.shared.dataTask(with: url){(data,response,error) in
                           if let data = data{

                               DispatchQueue.main.async {
                                   
                                   cell.headImage.image = UIImage(data: data)
                                   
                               }
                           }
                       }.resume()
                }
        
                    return cell
         
        }
                
        return UITableViewCell()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newsDetails" || segue.identifier == "newsDetails2" {
            let controller = segue.destination as! NewsDetailsVC
            
            controller.currentNews = DataProcessing.news[newsTable.indexPathForSelectedRow!.row];
            
            newsTable.deselectRow(at: newsTable.indexPathForSelectedRow!, animated: false)

        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
