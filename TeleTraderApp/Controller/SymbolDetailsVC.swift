//
//  SymbolDetailsVC.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/18/21.
//

import UIKit

class SymbolDetailsVC: UIViewController {
    @IBOutlet weak var lastLabel: UILabel!
    @IBOutlet weak var changeLabel: UILabel!
    @IBOutlet weak var changeProcentLabel: UILabel!
    @IBOutlet weak var currentDate: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var bidLabel: UILabel!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var lowLabel: UILabel!
    @IBOutlet weak var highLabel: UILabel!
    
    var currentSymbol:Symbol!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        initializeUI()
       
        title = currentSymbol.name
        // Do any additional setup after loading the view.
    }
        

    func initializeUI(){
       
           UITabBar.appearance().barTintColor = UIColor.blue
        self.navigationController?.isNavigationBarHidden = false
        
     
        lastLabel.text = String(currentSymbol.quote.last) + " \n \(currentSymbol.currency)"
        lastLabel.textColor = currentSymbol.quote.last > 0 ? .green : .red
        changeLabel.text = String(currentSymbol.quote.change)
        changeLabel.textColor = currentSymbol.quote.change > 0 ? .green : .red
        changeProcentLabel.text = String(currentSymbol.quote.changePercent) + "%"
        changeProcentLabel.textColor = currentSymbol.quote.changePercent > 0 ? .green : .red
        currentDate.text =  currentSymbol.quote.dateTime
        
        volumeLabel.text = String(currentSymbol.quote.volume)
        bidLabel.text = String(currentSymbol.quote.bid)
        askLabel.text = String(currentSymbol.quote.ask)
        highLabel.text = String(currentSymbol.quote.high)
        lowLabel.text = String(currentSymbol.quote.low)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
