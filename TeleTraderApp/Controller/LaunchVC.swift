//
//  LaunchVC.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/17/21.
//

import UIKit

class LaunchVC: UIViewController {

    @IBOutlet weak var loadButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // DoData any additional setup after loading the view.
        DataProcessing.getData(DataProcessing.jsonString)
        DataProcessing.getNews(DataProcessing.jsonNewsString)
        
        roundButtons()
    }
    

    func roundButtons(){
        loadButton.layer.cornerRadius = 10
        loadButton.layer.cornerRadius = 10
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
