//
//  News.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/19/21.
//

import Foundation

struct News:Codable{
    var headLine:String
    var picture:String
    
}
