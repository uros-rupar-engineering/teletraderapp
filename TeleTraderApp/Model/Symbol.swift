//
//  Symbol.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/17/21.
//

import Foundation

struct Symbol: Codable {
    let id, name, tickerSymbol, isin: String
    let currency, stockExchangeName, decorativeName: String
    let quote: Quote
}

// MARK: - Quote
struct Quote: Codable {
    let last, high, low, bid: Double
    let ask: Double
    let volume: Int
    let dateTime: String
    let change, changePercent: Double
}
