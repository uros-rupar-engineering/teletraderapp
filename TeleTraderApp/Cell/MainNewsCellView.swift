//
//  MainNewsCellView.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/19/21.
//

import UIKit

class MainNewsCellView: UITableViewCell {

    @IBOutlet weak var headLine: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
