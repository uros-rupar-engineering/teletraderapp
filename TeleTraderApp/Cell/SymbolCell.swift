//
//  SymbolCell.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/17/21.
//

import UIKit

class SymbolCell: UITableViewCell {

    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var symbolNameLabel: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
