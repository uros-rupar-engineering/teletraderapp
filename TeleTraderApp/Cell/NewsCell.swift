//
//  NewsCell.swift
//  TeleTraderApp
//
//  Created by uros.rupar on 9/19/21.
//

import UIKit

class NewsCell: UITableViewCell {
    @IBOutlet weak var headLineLabel: UILabel!
    
    @IBOutlet weak var headImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
